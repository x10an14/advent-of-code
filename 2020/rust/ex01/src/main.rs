// Std imports
use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>> {
    let mut expenses = fs::read_to_string(format!("{}/input.txt", env!("CARGO_PKG_NAME")))
        .expect("Input file 'input.txt' missing from crate root!")
        .lines()
        .filter(|line| "" != *line)
        .map(|line| match u32::from_str_radix(line, 10u32) {
            Ok(num) => num,
            _ => 0,
        })
        .collect::<Vec<_>>();
    expenses.sort();
    // dbg!(&expenses);

    let mut product = 0;
    for (index1, expense1) in expenses.iter().enumerate() {
        for (index2, expense2) in expenses.iter().enumerate().skip(index1 + 1) {
            for expense3 in expenses.iter().skip(index2 + 1) {
                let triple_sum = expense1 + expense2 + expense3;
                if triple_sum == 2020 {
                    product = expense1 * expense2 * expense3;
                }
                if triple_sum > 2020 {
                    break;
                }
            }
            if product != 0 {
                break;
            }
        }
        if product != 0 {
            break;
        }
    }
    //dbg!(&product);
    eprint!("Product: ");
    println!("{}", product);
    Ok(())
}
