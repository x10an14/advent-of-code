// Std-lib imports
use std::error::Error;
use std::fs;

mod boarding_pass {
    #[derive(Debug)]
    pub struct BoardingPass {
        row: u8,
        column: u8,
    }
    impl BoardingPass {
        pub fn seat_id(&self) -> u16 {
            self.row as u16 * 8 + self.column as u16
        }
    }

    static BASE: i32 = 2;
    fn calculate_value(input_str: &[char], max_power: u32, accumulator: u8) -> u8 {
        if input_str.len() == 1 {
            return accumulator
                + match input_str[0] {
                    'F' | 'L' => 0,
                    'B' | 'R' => 1,
                    _ => panic!("Element {:#?} invalid row!", input_str[0]),
                };
        }
        let (head, tail) = input_str.split_at(1);
        calculate_value(
            tail,
            max_power,
            accumulator
                + match head[0] {
                    'F' | 'L' => 0,
                    'B' | 'R' => BASE.pow(max_power - (max_power - (tail.len() as u32))) as u8,
                    _ => panic!("Element {:#?} invalid row!", input_str[0]),
                },
        )
    }
    pub fn new(input_string: &str) -> BoardingPass {
        let row_str = input_string.chars().take(7).collect::<Vec<_>>();
        debug_assert!(row_str.len() == 7);
        debug_assert!(row_str.iter().all(|chr| ['F', 'B'].contains(chr)));
        let column_str = input_string.chars().skip(7).take(3).collect::<Vec<_>>();
        debug_assert!(column_str.len() == 3);
        debug_assert!(column_str.iter().all(|chr| ['R', 'L'].contains(chr)));
        BoardingPass {
            row: calculate_value(&row_str, 6, 0),
            column: calculate_value(&column_str, 2, 0),
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    // Parse input
    let file_contents =
        fs::read_to_string(format!("{}/input.txt", env!("CARGO_PKG_NAME")))
        .expect("Input file 'input.txt' missing from crate root!");
    let mut boarding_pass_ids = file_contents
        .lines()
        .filter(|line| &"" != line)
        .map(|line| boarding_pass::new(line).seat_id())
        .collect::<Vec<_>>();
    boarding_pass_ids.sort();

    let mut previous = boarding_pass_ids[0];
    for id in boarding_pass_ids.iter().skip(1) {
        if id - previous > 1 {
            // eprintln!("{} vs {}!", previous, id);
            previous += 1;
            break;
        }
        previous = *id;
    }

    // Print result
    // dbg!(&passports);
    eprint!("#YourSeatId: ");
    println!("{:#?}", previous);
    Ok(())
}
