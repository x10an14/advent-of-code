// Std-lib imports
use std::error::Error;
use std::fs;

fn main() -> Result<(), Box<dyn Error>> {
    // Parse input/map
    let file_contents =
        fs::read_to_string(format!("{}/input.txt", env!("CARGO_PKG_NAME")))
        .expect("Input file 'input.txt' missing from crate root!");
    let tree_map = file_contents
        .lines()
        .filter(|line| "" != *line)
        .collect::<Vec<_>>();

    // Only instantiate constant _once_
    let tree_char = match "#".parse::<char>() {
        Ok(t) => t,
        Err(_) => panic!("Unable to convert '#' from String to char!"),
    };

    // Calculate
    let total_trees_hit = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        .iter()
        .map(|(right, down)| {
            tree_map
                .iter()
                .step_by(*down)
                .enumerate()
                .filter(|(index, line)| line.chars().cycle().nth(*index * right) == Some(tree_char))
                .count()
        })
        .fold(1u128, |accumulator, product_factor| {
            accumulator * (product_factor as u128)
        });

    // Print result
    // dbg!(total_trees_hit);
    eprint!("#TreesHit: ");
    println!("{}", total_trees_hit);
    Ok(())
}
