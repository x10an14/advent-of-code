// Std-lib imports
use std::collections::HashSet;
use std::error::Error;
use std::fs;
use std::iter::FromIterator;

fn calculate_score_for_group(group: &str) -> u32 {
    let mut common_answers_in_group: HashSet<char> =
        HashSet::from_iter(group.lines().nth(0).unwrap().chars());
    for next_persons_answers in group.lines().skip(1) {
        let answers = HashSet::from_iter(next_persons_answers.chars().to_owned());
        common_answers_in_group =
            HashSet::from_iter(common_answers_in_group.intersection(&answers).copied());
    }
    common_answers_in_group.len() as u32
}

fn main() -> Result<(), Box<dyn Error>> {
    // Parse input
    let file_contents =
        fs::read_to_string(format!("{}/input.txt", env!("CARGO_PKG_NAME")))
        .expect("Input file 'input.txt' missing from crate root!");
    let counts_sum: u32 = file_contents
        .split("\n\n")
        .map(|group| calculate_score_for_group(group))
        .sum();

    // Print result
    // dbg!(&passports);
    eprint!("#CountsSum: ");
    println!("{:#?}", counts_sum);
    Ok(())
}
