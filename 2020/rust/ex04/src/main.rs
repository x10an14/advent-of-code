// Std-lib imports
use std::error::Error;
use std::fs;

// 3rd-party imports
#[macro_use]
extern crate lazy_static;

mod structs;
use structs::passport;

fn main() -> Result<(), Box<dyn Error>> {
    // Parse input
    let file_contents =
        fs::read_to_string("input.txt").expect("Input file 'input.txt' missing from crate root!");
    let passports = file_contents
        .split("\n\n")
        .map(|passport_string| passport::new(passport_string))
        .filter_map(|pass| pass)
        .collect::<Vec<_>>();

    // Print result
    // dbg!(&passports);
    eprint!("#ValidPassports: ");
    println!("{}", passports.len());
    Ok(())
}
