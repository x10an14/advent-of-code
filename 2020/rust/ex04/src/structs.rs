// 3rd-party imports
use fancy_regex::Regex;

mod height {
    use super::Regex;

    #[derive(Debug)]
    pub enum HeightType {
        Metric,
        Imperial,
    }

    #[derive(Debug)]
    pub struct Height {
        kind: HeightType,
        value: u8,
    }

    lazy_static! {
        static ref HEIGHT_REGEX: Regex =
            Regex::new(r"hgt:(?P<value>\d{2,3})(?P<kind>cm|in)").unwrap();
    }

    pub fn new(input: &str) -> Option<Height> {
        let captures = match HEIGHT_REGEX.captures(input) {
            Ok(m) => match m {
                Some(captures) => captures,
                _ => return None,
            },
            _ => return None,
        };

        let kind = match &captures.name("kind") {
            Some(kind_str) => match kind_str.as_str() {
                "cm" => HeightType::Metric,
                "in" => HeightType::Imperial,
                _ => return None,
            },
            _ => return None,
        };

        // Heightlimits
        let value = match &captures.name("value") {
            Some(value_str) => match value_str.as_str().parse::<u8>() {
                Ok(val) => val,
                _ => return None,
            },
            _ => return None,
        };
        match kind {
            HeightType::Metric => match value {
                150..=193 => {} // Is ok, continue
                _ => return None,
            },
            HeightType::Imperial => match value {
                59..=76 => {} // Is ok, continue
                _ => return None,
            },
        };

        Some(Height {
            kind: kind,
            value: value,
        })
    }
}

mod year {
    use super::Regex;
    #[derive(Debug)]
    pub struct Year(u16);
    lazy_static! {
        static ref BORN_YEAR_REGEX: Regex = Regex::new(r"(?P<kind>byr):(?P<value>\d{4})").unwrap();
        static ref ISSUED_YEAR_REGEX: Regex =
            Regex::new(r"(?P<kind>iyr):(?P<value>\d{4})").unwrap();
        static ref EXPIRATION_YEAR_REGEX: Regex =
            Regex::new(r"(?P<kind>eyr):(?P<value>\d{4})").unwrap();
    }
    pub fn new(input_string: &str, variant: &str) -> Option<Year> {
        let captures = match variant {
            "byr" => match BORN_YEAR_REGEX.captures(input_string) {
                Ok(c) => match c {
                    Some(captures) => captures,
                    _ => return None,
                },
                _ => return None,
            },
            "iyr" => match ISSUED_YEAR_REGEX.captures(input_string) {
                Ok(c) => match c {
                    Some(captures) => captures,
                    _ => return None,
                },
                _ => return None,
            },
            "eyr" => match EXPIRATION_YEAR_REGEX.captures(input_string) {
                Ok(c) => match c {
                    Some(captures) => captures,
                    _ => return None,
                },
                _ => return None,
            },
            _ => return None,
        };

        let value = match &captures.name("value") {
            Some(string) => match u16::from_str_radix(string.as_str(), 10) {
                Ok(value) => value,
                _ => return None,
            },
            _ => return None,
        };

        if !((variant == "byr" && value >= 1920 && value <= 2002)
            || (variant == "iyr" && value >= 2010 && value <= 2020)
            || (variant == "eyr" && value >= 2020 && value <= 2030))
        {
            return None;
        }
        Some(Year(value))
    }
}

mod eye_color {
    use super::Regex;

    #[derive(Debug)]
    pub enum Color {
        Amber,
        Blue,
        Brown,
        Grey,
        Green,
        Hazel,
        Other,
    }
    lazy_static! {
        static ref COLOR_REGEX: Regex =
            Regex::new("ecl:(?P<value>amb|blu|brn|gry|grn|hzl|oth)").unwrap();
    }
    pub fn new(input_string: &str) -> Option<Color> {
        let captures = match COLOR_REGEX.captures(input_string) {
            Ok(c) => match c {
                Some(captures) => captures,
                _ => return None,
            },
            _ => return None,
        };
        let value = match &captures.name("value") {
            Some(v) => match v.as_str() {
                "amb" => Color::Amber,
                "blu" => Color::Blue,
                "brn" => Color::Brown,
                "gry" => Color::Grey,
                "grn" => Color::Green,
                "hzl" => Color::Hazel,
                "oth" => Color::Other,
                _ => return None,
            },
            _ => return None,
        };
        Some(value)
    }
}

mod hair_color {
    use super::Regex;

    #[derive(Debug)]
    pub struct Color(u32);
    lazy_static! {
        static ref COLOR_REGEX: Regex = Regex::new(r"hcl:#(?P<value>\h{6})(?!\h)").unwrap();
    }
    pub fn new(input_string: &str) -> Option<Color> {
        let captures = match COLOR_REGEX.captures(input_string) {
            Ok(c) => match c {
                Some(captures) => captures,
                _ => return None,
            },
            _ => return None,
        };
        match &captures.name("value") {
            Some(v) => match u32::from_str_radix(v.as_str(), 16) {
                Ok(v) => Some(Color(v)),
                _ => None,
            },
            _ => None,
        }
    }
}

pub mod passport {
    use super::{eye_color, hair_color, height, year, Regex};
    #[derive(Debug)]
    pub struct Passport {
        /// Birth year
        byr: year::Year,
        /// Issue year
        iyr: year::Year,
        /// Expiration year
        eyr: year::Year,
        /// Height
        hgt: height::Height,
        /// Hair color
        hcl: hair_color::Color,
        /// Eye color
        ecl: eye_color::Color,
        /// Passport ID
        pid: u32,
    }
    lazy_static! {
        static ref PASSPORT_PIN_REGEX: Regex = Regex::new(r"pid:(?P<value>\d{9})(?!\d)").unwrap();
    }
    pub fn new(input_string: &str) -> Option<Passport> {
        let captures = match PASSPORT_PIN_REGEX.captures(input_string) {
            Ok(c) => match c {
                Some(captures) => captures,
                _ => return None,
            },
            _ => return None,
        };
        Some(Passport {
            byr: match year::new(input_string, "byr") {
                Some(v) => v,
                _ => return None,
            },
            iyr: match year::new(input_string, "iyr") {
                Some(v) => v,
                _ => return None,
            },
            eyr: match year::new(input_string, "eyr") {
                Some(v) => v,
                _ => return None,
            },
            hgt: match height::new(input_string) {
                Some(v) => v,
                _ => return None,
            },
            hcl: match hair_color::new(input_string) {
                Some(v) => v,
                _ => return None,
            },
            ecl: match eye_color::new(input_string) {
                Some(v) => v,
                _ => return None,
            },
            pid: match &captures.name("value") {
                Some(v) => match u32::from_str_radix(v.as_str(), 10) {
                    Ok(v) => v,
                    _ => return None,
                },
                _ => return None,
            },
        })
    }
}
