// Std-lib imports
use std::error::Error;
use std::fs;

// 3rd-party imports
use itertools::Itertools;

fn is_password_valid(password: &str, rule_string: &str) -> bool {
    let (range, letter) = rule_string
        .splitn(2, ' ')
        .collect_tuple()
        .expect("<rule_string> must be of the form '<range> <character>'.");

    // Get indices valid for letter
    let indices = range
        .split('-')
        .map(|index| match usize::from_str_radix(index, 10) {
            Ok(num) => num,
            _ => usize::MAX,
        })
        .map(|index| index - 1)
        .collect::<Vec<_>>();

    // Count letters in password which match letter specified by rule
    let char_letter = letter
        .chars()
        .nth(0)
        .expect("Only one alphabetical character in rule.");
    let occurrences = password
        .chars()
        .enumerate()
        .filter(|(index, _)| indices.contains(index))
        .filter(|(_, character)| character == &char_letter)
        .count();

    occurrences == 1
}

fn main() -> Result<(), Box<dyn Error>> {
    let valid_passwords = fs::read_to_string(format!("{}/input.txt", env!("CARGO_PKG_NAME")))
        .expect("Input file 'input.txt' missing from crate root!")
        .lines()
        .filter(|line| "" != *line)
        .map(|line| {
            line.splitn(2, ": ")
                .collect_tuple::<(&str, &str)>()
                .expect("Lines must be of the form '<rule_string>: <password>'.")
        })
        .filter(|(rule, pw)| is_password_valid(pw, rule))
        .count();
    eprint!("#ValidPasswords: ");
    println!("{}", valid_passwords);
    Ok(())
}
